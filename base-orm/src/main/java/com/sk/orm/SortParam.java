package com.sk.orm;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

/**
 * 排序参数
 *
 * @author smy
 * {@code @date} 2022/10/26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "排序查询模型")
public class SortParam {
    @Schema(description = "排序字段")
    private String sortField;

    @Schema(description = "排序方式", allowableValues = "asc,desc")
    private String sortOrder;

    public SortParam(String sortField) {
        this.sortField = sortField;
    }

    public boolean checkSort() {
        return StringUtils.hasText(sortField);
    }
}
