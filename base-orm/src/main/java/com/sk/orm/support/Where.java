package com.sk.orm.support;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 查询条件标签。
 *
 * @author smy
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Where {

    WhereType value() default WhereType.eq;

    String[] keys() default "";

    boolean not() default false;

    boolean having() default false;
}
