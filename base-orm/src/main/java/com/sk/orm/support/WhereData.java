package com.sk.orm.support;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

/**
 * where条件数据封装
 *
 * @author smy
 * {@code @date} 2022/11/2
 */
@Getter
@ToString
public class WhereData {
    private final WhereType type;
    private final String key;
    private final Object value;
    private boolean not = false;


    protected WhereData(WhereType type, String key, Object value) {
        this.type = type;
        this.key = key;
        this.value = value;
    }

    public WhereData not() {
        this.not = true;
        return this;
    }

    public static WhereData isNull(String name) {
        return new WhereData(WhereType.isNull, name, true);
    }

    public static WhereData or(Collection<WhereData> value) {
        return new WhereData(WhereType.or, null, value);
    }

    public static WhereData or(WhereData... value) {
        return new WhereData(WhereType.or, null, Arrays.asList(value));
    }

    public static WhereData and(Collection<WhereData> value) {
        return new WhereData(WhereType.and, null, value);
    }

    public static WhereData and(WhereData... value) {
        return new WhereData(WhereType.and, null, Arrays.asList(value));
    }

    public static WhereData eq(String name, Object value) {
        return new WhereData(WhereType.eq, name, value);
    }

    public static WhereData like(String name, String value) {
        return new WhereData(WhereType.like, name, value);
    }

    public static WhereData leftLike(String name, String value) {
        return new WhereData(WhereType.leftLike, name, value);
    }

    public static WhereData rightLike(String name, String value) {
        return new WhereData(WhereType.rightLike, name, value);
    }

    public static WhereData gt(String name, Comparable<?> value) {
        return new WhereData(WhereType.gt, name, value);
    }

    public static WhereData ge(String name, Comparable<?> value) {
        return new WhereData(WhereType.ge, name, value);
    }

    public static WhereData lt(String name, Comparable<?> value) {
        return new WhereData(WhereType.lt, name, value);
    }

    public static WhereData le(String name, Comparable<?> value) {
        return new WhereData(WhereType.le, name, value);
    }

    public static WhereData in(String name, Collection<?> value) {
        return new WhereData(WhereType.in, name, value);
    }

    public static WhereData in(String name, Object... value) {
        Collection<Object> collection = new ArrayList<>(Arrays.asList(value));
        return new WhereData(WhereType.in, name, collection);
    }

    public static WhereData query(String query) {
        return new WhereData(WhereType.query, query, null);
    }

    public static WhereData query(String query, Map<String, Object> param) {
        return new WhereData(WhereType.query, query, param);
    }


}
