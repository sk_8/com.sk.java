package com.sk.orm.support;

/**
 * 查询支持类型。
 *
 * @author smy
 */
public enum WhereType {
    eq,
    like,
    /**
     * 左模糊
     */
    leftLike,
    /**
     * 右模糊
     */
    rightLike,
    gt,
    ge,
    lt,
    le,
    in,
    isNull,
    or,
    and,
    /**
     * 查询语句,不同实现类实现
     */
    query;

}
