package com.sk.orm.support;

import java.lang.annotation.*;

/**
 * 级联查询标签。
 *
 * @author smy
 */
@Target({ElementType.TYPE, ElementType.METHOD,ElementType.CONSTRUCTOR})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(value = Join.List.class)
public @interface Join {
    String value();

    /**
     * 多注解支持。
     */
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    @interface List {
        Join[] value();
    }
}
