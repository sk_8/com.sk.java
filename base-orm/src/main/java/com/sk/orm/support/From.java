package com.sk.orm.support;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 主表查询标识。
 *
 * @author smy
 * {@code @date} 2023/12/18
 */
@Target({ElementType.TYPE, ElementType.METHOD,ElementType.CONSTRUCTOR})
@Retention(RetentionPolicy.RUNTIME)
public @interface From {
    Class value();

    String as() default Criteria.sql_def_from_as;

}
