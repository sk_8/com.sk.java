package com.sk.orm.support;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author smy
 * {@code @date} 2023/12/18
 */
@Target({ElementType.TYPE, ElementType.METHOD,ElementType.CONSTRUCTOR})
@Retention(RetentionPolicy.RUNTIME)
public @interface GroupBy {
    String value();
}
