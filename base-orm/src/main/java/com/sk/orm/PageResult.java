package com.sk.orm;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 分页结果返回值
 *
 * @author smy
 * {@code @date} 2022/10/26
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "分页查询返回值")
public class PageResult<T> {
    @Schema(description = "总数")
    private Long total;
    @Schema(description = "查询内容列表")
    private List<T> content;
}
