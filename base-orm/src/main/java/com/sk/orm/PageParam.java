package com.sk.orm;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;


/**
 * 分页查询参数
 *
 * @author smy
 * {@code @date} 2022/10/26
 */
@Data
@Schema(description = "分页查询参数")
public class PageParam extends SortParam {
    @Schema(description = "页号", example = "1")
    private Integer pageNo = 1;
    @Schema(description = "页大小", example = "10")
    private Integer pageSize = 10;

}
