package com.sk.orm;

import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


/**
 * CommonDao基础业务实例
 *
 * @author smy
 * {@code @date} 2022/10/26
 */
@Repository("commonDao")
public class CommonDaoImpl extends CommonDaoAbstract {
    @Autowired
    protected EntityManager entityManager;

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }
}
