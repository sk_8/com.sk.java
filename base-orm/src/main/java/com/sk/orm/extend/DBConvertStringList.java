package com.sk.orm.extend;

/**
 * @author smy
 * {@code @date} 2024/7/3
 */
public class DBConvertStringList extends BaseDBConvertList<String> {
    public DBConvertStringList() {
        super(String::toString);
    }
}
