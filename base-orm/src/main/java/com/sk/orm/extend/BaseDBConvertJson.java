package com.sk.orm.extend;

import com.alibaba.fastjson.JSONObject;
import jakarta.persistence.AttributeConverter;


/**
 * @author smy
 * {@code @date} 2022/11/6
 */
public class BaseDBConvertJson<T> implements AttributeConverter<T, String> {
    private final Class<T> c;

    public BaseDBConvertJson(Class<T> c) {
        this.c = c;
    }

    @Override
    public final String convertToDatabaseColumn(T attribute) {
        return attribute == null ? null : JSONObject.toJSONString(attribute);
    }

    @Override
    public final T convertToEntityAttribute(String dbData) {
        return dbData == null ? null : JSONObject.parseObject(dbData, c);
    }
}
