/**
 * jpa扩展用法
 * <p>
 * 1、基于AttributeConverter的自定义数据类型转换
 *
 * @Convert(converter = DBConvertStringList.class)
 * @Column(columnDefinition = "text")
 * private List<String> labelList;
 *
 * @author smy
 * {@code @date} 2023/4/16
 */
package com.sk.orm.extend;
