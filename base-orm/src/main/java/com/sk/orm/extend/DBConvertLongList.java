package com.sk.orm.extend;

/**
 * @author smy
 * {@code @date} 2024/7/3
 */
public class DBConvertLongList extends BaseDBConvertList<Long> {
    public DBConvertLongList() {
        super(Long::valueOf);
    }
}
