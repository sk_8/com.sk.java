package com.sk.orm.extend;

/**
 * @author smy
 * {@code @date} 2024/7/3
 */
public class DBConvertIntList extends BaseDBConvertList<Integer> {
    public DBConvertIntList() {
        super(Integer::valueOf);
    }
}
