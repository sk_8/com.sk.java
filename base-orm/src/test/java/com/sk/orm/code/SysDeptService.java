
package com.sk.orm.code;

import com.sk.orm.CommonDao;
import com.sk.orm.PageParam;
import com.sk.orm.PageResult;
import com.sk.orm.SortParam;
import com.sk.orm.support.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jakarta.transaction.Transactional;
import java.util.List;

@Service
public class SysDeptService{
    @Autowired
    private CommonDao commonDao;
    
    public PageResult<SysDept> page(SysDeptQuery query, PageParam pageParam) {
        Criteria<SysDept> criteria = Criteria.whereByTag(SysDept.class, query).page(pageParam);
        return commonDao.page(criteria);
    }
    public List<SysDept> list(SysDeptQuery query, SortParam sortParam) {
        Criteria<SysDept> criteria = Criteria.whereByTag(SysDept.class, query).orderBy(sortParam);
        return commonDao.list(criteria);
    }
    public SysDept get(Long deptId) {
        return commonDao.find(SysDept.class, deptId);
    }
    @Transactional
    public Long add(SysDept data) {
        commonDao.add(data);
        return data.getDeptId();
    }
    @Transactional
    public void save(SysDept data) {
        commonDao.update(data);
    }
    @Transactional
    public void delete(Long deptId) {
        commonDao.delete(SysDept.class, deptId);
    }
}