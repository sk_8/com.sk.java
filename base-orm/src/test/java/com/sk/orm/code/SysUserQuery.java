
package com.sk.orm.code;

import com.sk.orm.support.Where;
import com.sk.orm.support.WhereType;
import lombok.Data;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "系统用户表查询条件")
@Data
public class SysUserQuery {
    @Where(value = WhereType.like, name = {"nickname", "e1.deptName"})
    private String search;
    @Where
    private Integer userState;
}