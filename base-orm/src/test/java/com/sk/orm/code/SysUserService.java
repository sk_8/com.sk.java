
package com.sk.orm.code;

import com.sk.orm.CommonDao;
import com.sk.orm.PageParam;
import com.sk.orm.PageResult;
import com.sk.orm.SortParam;
import com.sk.orm.support.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import jakarta.transaction.Transactional;

import java.util.List;

@Service
public class SysUserService {
    @Autowired
    private CommonDao commonDao;

    public PageResult<SysUserVo> page(SysUserQuery query, PageParam pageParam) {
        Criteria<SysUserVo> criteria = Criteria.whereByTag(SysUserVo.class, query).page(pageParam);
        return commonDao.page(criteria);
    }

    public List<SysUserVo> list(SysUserQuery query, SortParam sortParam) {
        Criteria<SysUserVo> criteria = Criteria.whereByTag(SysUserVo.class, query).orderBy(sortParam);
        return commonDao.list(criteria);
    }

    public SysUserVo get(Long userId) {
        return commonDao.find(Criteria.eq(SysUserVo.class, "userId", userId));
    }

    @Transactional
    public Long add(SysUser data) {
        commonDao.add(data);
        return data.getUserId();
    }

    @Transactional
    public void save(SysUser data) {
        commonDao.update(data);
    }

    @Transactional
    public void delete(Long userId) {
        commonDao.delete(SysUser.class, userId);
    }
}