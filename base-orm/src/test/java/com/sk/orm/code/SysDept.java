
package com.sk.orm.code;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

import java.util.Date;

@Data
@Schema(description = "部门表")
@Entity
public class SysDept {
    @Id
    @Schema(description = "部门ID")
    private Long deptId;
    
    @Schema(description = "创建人ID")
    private Long deptCreatedBy;
    
    @Schema(description = "创建时间")
    private Date deptCreatedTime;
    
    @Schema(description = "修改人ID")
    private Long deptUpdatedBy;
    
    @Schema(description = "更新时间")
    private Date deptUpdatedTime;
    
    @Schema(description = "部门状态:dict=state_common")
    private Integer deptState;
    
    @Schema(description = "上级部门ID")
    private Long deptParentId;
    
    @Schema(description = "部门名称")
    private String deptName;

}