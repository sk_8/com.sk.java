package com.sk.orm.code;

import com.sk.orm.support.Join;
import com.sk.orm.support.Select;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.ToString;
import org.springframework.beans.BeanUtils;

/**
 * @author smy
 * {@code @date} 2024/5/7
 */
@Join("left join SysDept as e1 on e0.deptId=e1.deptId")
@Getter
@ToString(callSuper = true)
public class SysUserVo extends SysUser {
    @Schema(description = "部门名称")
    private String deptName;

    public SysUserVo(@Select("e0") SysUser user,
                     @Select("e1.deptName") String deptName) {
        BeanUtils.copyProperties(user,this);
        this.deptName = deptName;
    }
}
