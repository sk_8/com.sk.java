
package com.sk.orm.code;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

import java.util.Date;

@Data
@Schema(description = "系统用户表")
@Entity
public class SysUser {
    @Id
    @Schema(description = "用户ID")
    private Long userId;
    
    @Schema(description = "创建人ID")
    private Long userCreatedBy;
    
    @Schema(description = "创建时间")
    private Date userCreatedTime;
    
    @Schema(description = "修改人ID")
    private Long userUpdatedBy;
    
    @Schema(description = "更新时间")
    private Date userUpdatedTime;
    
    @Schema(description = "状态:dict=state_common")
    private Integer userState;
    
    @Schema(description = "用户昵称")
    private String nickname;
    
    @Schema(description = "账号名")
    private String username;
    
    @Schema(description = "密码摘要")
    private String passwordDigest;
    
    @Schema(description = "机构ID")
    private Long deptId;

}