package com.sk.orm.code;

import com.sk.orm.support.From;
import com.sk.orm.support.GroupBy;
import com.sk.orm.support.Join;
import com.sk.orm.support.Select;
import com.sk.util.convert.ConvertPoint;
import com.sk.util.convert.LongConverter;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author smy
 * {@code @date} 2024/5/8
 */
@Data
public class UserDeptCount {
    @Schema(description = "部门ID")
    private Long deptId;

    @Schema(description = "部门名称")
    private String deptName;
    @Schema(description = "用户数量")
    private Long userCount;
    @Schema(description = "用户ID平均值")
    private Long userIdAvg;

    @From(SysUser.class)
    @Join("left join SysDept as e1 on e0.deptId=e1.deptId")
    @GroupBy("e0.deptId")
    public UserDeptCount(@Select("e0.deptId") Long deptId,
                         @Select("e1.deptName") String deptName,
                         @Select("count(1)") Long userCount,
                         @Select("avg(userId)") @ConvertPoint(LongConverter.class) Long userIdAvg) {
        this.deptId = deptId;
        this.deptName = deptName;
        this.userCount = userCount;
        this.userIdAvg = userIdAvg;
    }
}
