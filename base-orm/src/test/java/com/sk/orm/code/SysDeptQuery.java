
package com.sk.orm.code;

import lombok.Data;
import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "部门表查询条件")
@Data
public class SysDeptQuery{
}