package com.sk.orm;

import com.sk.orm.code.*;
import com.sk.orm.support.Criteria;
import com.sk.orm.support.WhereData;
import jakarta.annotation.Resource;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Objects;

/**
 * @author smy
 * {@code @date} 2022/11/5
 */
@SpringBootTest(classes = OrmApp.class)
@Slf4j
public class CommonDaoTest {

    @Resource
    private CommonDao commonDao;
    @Resource
    private SysUserService service;

    @Test
    public void testBase() {
        SysUser user1 = new SysUser();
        user1.setUserId(100L);
        user1.setUserState(1);
        user1.setUsername("test");
        Long id = service.add(user1);
        SysUser user2 = service.get(id);
        Assert.isTrue(Objects.equals(user1.getUsername(), user2.getUsername()), "返回信息不一致");
        service.delete(id);
        SysUser user3 = service.get(id);
        Assert.isNull(user3, "用户已删除,user3应为null");
    }

    @Test
    public void testList() {
        PageResult<SysDept> deptPageResult = commonDao.page(Criteria.gen(SysDept.class).asc("deptId"));
        log.info("========>deptList={}", deptPageResult.getTotal());
        deptPageResult.getContent().forEach(System.out::println);
        PageResult<SysUserVo> userPageResult = commonDao.page(Criteria.gen(SysUserVo.class));
        log.info("========>userList={}", userPageResult.getTotal());
        userPageResult.getContent().forEach(System.out::println);
    }

    @Test
    public void testQuery() {
        SysUserQuery query = new SysUserQuery();
        query.setSearch("二级部门");
        PageResult<SysUserVo> userPageResult = commonDao.page(Criteria.whereByTag(SysUserVo.class, query));
        log.info("========>userList={}", userPageResult.getTotal());
        userPageResult.getContent().forEach(System.out::println);
    }

    @Test
    public void testGroup() {
        SysUserQuery query = new SysUserQuery();
        query.setUserState(1);
        Criteria<UserDeptCount> criteria = Criteria.whereByTag(UserDeptCount.class, query);
        criteria.having(WhereData.gt("count(1)", 0));
        List<UserDeptCount> list = commonDao.list(criteria);
        System.out.println("=================>");
        list.forEach(System.out::println);
    }


}
