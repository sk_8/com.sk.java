package com.sk.orm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author smy
 * {@code @date} 2022/10/26
 */
@SpringBootApplication
public class OrmApp {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(OrmApp.class);
    }
}
