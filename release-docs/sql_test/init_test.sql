/*
 Navicat Premium Data Transfer
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
                             `dept_id` bigint NOT NULL COMMENT '部门ID',
                             `dept_created_by` bigint  COMMENT '创建人ID',
                             `dept_created_time` datetime  COMMENT '创建时间',
                             `dept_updated_by` bigint  COMMENT '修改人ID',
                             `dept_updated_time` datetime  COMMENT '更新时间',
                             `dept_state` int  COMMENT '部门状态:state_common',
                             `dept_parent_id` bigint  COMMENT '上级部门ID',
                             `dept_name` varchar(100)   COMMENT '部门名称',
                             PRIMARY KEY (`dept_id`) USING BTREE
)  COMMENT = '部门表' ;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1, 1, '2024-01-01 00:00:00', 1, '2024-01-01 00:00:00', 1, NULL, '顶级部门');
INSERT INTO `sys_dept` VALUES (2, 1, '2024-01-01 00:00:00', 1, '2024-01-01 00:00:00', 1, 1, '二级部门');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
                             `user_id` bigint NOT NULL COMMENT '用户ID',
                             `user_created_by` bigint  COMMENT '创建人ID',
                             `user_created_time` datetime  COMMENT '创建时间',
                             `user_updated_by` bigint  COMMENT '修改人ID',
                             `user_updated_time` datetime  COMMENT '更新时间',
                             `user_state` int  COMMENT '状态:state_common',
                             `nickname` varchar(100)   COMMENT '用户昵称',
                             `username` varchar(100)   COMMENT '账号名',
                             `password_digest` varchar(100)   COMMENT '密码摘要',
                             `dept_id` bigint  COMMENT '机构ID',
                             PRIMARY KEY (`user_id`) USING BTREE
)  COMMENT = '系统用户表' ;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 1, '2024-01-01 00:00:00', 1, '2024-01-01 00:00:00', 1, '超级管理员', 'admin', NULL, 1);
INSERT INTO `sys_user` VALUES (2, 1, '2024-01-01 00:00:00', 1, '2024-01-01 00:00:00', 1, '部门管理员1', 'demo', NULL, 2);
INSERT INTO `sys_user` VALUES (3, 1, '2024-01-01 00:00:00', 1, '2024-01-01 00:00:00', 1, '部门管理员2', 'demo', NULL, 2);

SET FOREIGN_KEY_CHECKS = 1;
