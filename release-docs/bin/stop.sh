#!/bin/bash
BASE_DIR=$(cd `dirname $0`; cd ..;pwd)
APP_JAR=$(find ${BASE_DIR} -maxdepth 1 -name *.jar)
PID=$(ps -ef | grep "${APP_JAR}" | grep -v grep | awk '{ print $2 }')
time_over=10
if [ -z "${PID}" ]; then
  echo "${APP_JAR} has already stopped."
else
  echo "kill ${APP_JAR}"
  kill ${PID} && echo "Send stopping signal to server successfully."
  LOOPS=0
  while(true)
  do
    PID=$(ps -ef | grep "${APP_JAR}" | grep -v grep | awk '{ print $2 }')
    if [ -z "${PID}" ]; then
      echo "Stop server successfully! Cost ${LOOPS} seconds."
      break;
    fi
    if [ "${LOOPS}" -gt ${time_over} ]; then
      echo "$Stop server cost time over ${time_over} seconds. Now force stop it."
      kill -9 ${PID} && echo "Force stop successfully."
      break;
    else
      echo "Cost ${LOOPS} seconds."
    fi
    let LOOPS=LOOPS+1
    sleep 1
  done
fi
