#!/bin/bash
BASE_DIR=$(cd `dirname $0`; cd ..;pwd)
APP_JAR=$(find ${BASE_DIR} -maxdepth 1 -name *.jar)
JAVA_OPTIONS="-server -Xms500m -Xmx2g -Xmn256m -Xss256k -XX:MetaspaceSize=16m"
count=$(ps -ef|grep java|grep ${APP_JAR}|grep -v grep|wc -l)
if [ ${count} != 0 ]; then
  echo "${APP_JAR} is already running..."
  exit 1
else
  echo "${APP_JAR} is started successfully"
  nohup java ${JAVA_OPTIONS} -jar ${APP_JAR} >/dev/null 2>&1 &
fi
