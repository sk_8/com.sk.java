package com.sk.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author smy
 * {@code @date} 2022/11/25
 */
@SpringBootApplication(scanBasePackages = "com.sk")
public class WebDemoApp {

    public static void main(String[] args) {
        SpringApplication.run(WebDemoApp.class, args);
        WebConfigUtil.outWebDevInfo();
    }
}
