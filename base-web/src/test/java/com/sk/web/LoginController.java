package com.sk.web;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sk.util.ImgUtil;
import com.sk.util.StringUtil;
import com.sk.util.model.Result;
import com.sk.web.aop.AccessPoint;
import com.sk.web.support.LoginParam;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.awt.image.BufferedImage;

/**
 * @author smy
 * {@code @date} 2023/1/29
 */
@RestController
@RequestMapping("api/index")
@AccessPoint
@Slf4j
@Tag(name = "登录接口", description = "这是测试接口")
public class LoginController {
    @Resource
    private HttpServletRequest request;

    @GetMapping(value = "login/captcha.jpg", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] captcha() {
        String captcha = StringUtil.generate(4);
        log.info("验证码生成:{}", captcha);
        request.getSession().setAttribute("captcha", captcha);
        BufferedImage image = ImgUtil.gemCaptcha(200, 50, captcha);
        return ImgUtil.image2Bytes(image, "jpg");
    }

    @Operation(summary = "登录")
    @PostMapping(value = "login")
    public Result login(@RequestBody LoginParam param) {
        log.info("接口获取param={}", JSON.toJSONString(param));
        return Result.success(param);
    }
}
