package com.sk.web;

import com.sk.util.SpringUtil;
import com.sk.util.model.Result;
import com.sk.util.model.BusinessException;
import com.sk.web.aop.AccessPoint;
import com.sk.web.support.UserBO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.*;

/**
 * @author smy
 * {@code @date} 2022/11/25
 */
@RestController
@RequestMapping("base")
@AccessPoint
@Slf4j
@Tag(name = "基础接口")
public class BaseController {


    @Autowired
    WebApplicationContext applicationContext;

    @GetMapping("/getAllUrl")
    @Operation(summary = "获取spring-web全部路径", description = "XXXX说明文档")
    @AccessPoint(result = true)
    public Set<String> getAllUrl() {
        RequestMappingHandlerMapping mapping = applicationContext.getBean("requestMappingHandlerMapping", RequestMappingHandlerMapping.class);
        //获取url与类和方法的对应信息
        Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();
        Set<String> urls = new HashSet<>();
        for (Map.Entry<RequestMappingInfo, HandlerMethod> entry : map.entrySet()) {
            urls.addAll(entry.getKey().getPatternsCondition().getPatterns());
        }
        return urls;
    }

    @GetMapping("lang")
    public Result<String> lang(String key, Date abc) {
        if (!StringUtils.hasText(key)) {
            key = "result_success";
        }
        return Result.success(SpringUtil.getMessage("未找到i18n参数", key));
    }

    @GetMapping("error")
    @Operation(summary = "异常测试接口", responses = {@ApiResponse(responseCode = "200-10001", description = "验证码错误"), @ApiResponse(responseCode = "200-10002", description = "账号密码错误"),})
    public Result<String> error(@RequestParam("code") Integer code) {
        if (10001 == code) {
            throw new BusinessException(10001, "验证码错误");
        }
        if (10002 == code) {
            throw new BusinessException(10002, "账号密码错误");
        }
        throw new RuntimeException("异常接口测试");
    }

    @GetMapping("timeout")
    public Result timeout(Long sleep) throws InterruptedException {
        log.info("timeout start");
        sleep = Objects.requireNonNullElse(sleep, 70 * 1000L);
        Thread.sleep(sleep);
        log.info("timeout end");
        return Result.success();
    }

    @GetMapping("path/{name}")
    public Result<String> path(@PathVariable("name") String name) {
        String hello = "Hello " + name;
        return Result.success(hello);
    }

    @Operation(summary = "表单模式")
    @PostMapping(value = "form", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Result<UserBO> form(@Validated UserBO bo) {
        log.info("bo={}", bo);
        UserBO bo2 = new UserBO();
        BeanUtils.copyProperties(bo, bo2);
        return Result.success(bo2);
    }

    @PostMapping("json")
    public Result<UserBO> json(@Valid @RequestBody UserBO bo) {
        log.info("bo={}", bo);
        UserBO bo2 = new UserBO();
        BeanUtils.copyProperties(bo, bo2);
        return Result.success(bo2);
    }
}
