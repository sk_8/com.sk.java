package com.sk.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 自定义接口前缀
 *
 * @author smy
 * {@code @date} 2022/12/2
 */
@Configuration
public class MyConfig implements WebMvcConfigurer {
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.addPathPrefix("api", s -> {
                    if (s.isAnnotationPresent(RequestMapping.class) && s.getName().startsWith("com.sk.web")) {
                        String url = s.getAnnotation(RequestMapping.class).value()[0];
                        return !url.startsWith("api") && !url.startsWith("/api");
                    }
                    return false;
                }
        );
    }
}
