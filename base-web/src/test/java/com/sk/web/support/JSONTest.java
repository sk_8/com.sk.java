package com.sk.web.support;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.util.List;

/**
 * @author smy
 * {@code @date} 2023/5/23
 */
@Data
public class JSONTest {
    @JsonIgnore
    private String name;
    @JsonIgnore
    private boolean flag;

    public static void main(String[] args) {
        JSONTest test = new JSONTest();
        test.setName("123");
        System.out.println(test);
        System.out.println(JSONObject.toJSON(test));

        String value = "{\"nodeKey\": \"offline_accept\", \"orderId\": \"863046220710944\", \"nodeType\": \"common\", \"upload_images\": [\"f/b/2023/5/9e058828-2b63-46f8-ad63-c160d97bb0a8.jpeg\"]}";
        JSONObject jsonObject = JSONObject.parseObject(value);
        JSONArray jsonArray = jsonObject.getJSONArray("upload_images");
        System.out.println(jsonArray.toJavaList(String.class));
        List<String> list = JSONArray.parseArray(jsonObject.getString("upload_images"), String.class);
        System.out.println(list);
    }
}
