package com.sk.web.support;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.*;
import lombok.Data;

import java.util.Date;

/**
 * @author smy
 * {@code @date} 2023/1/16
 */
@Data
@Schema(description = "用户模型2")
public class UserBO {
    @Schema(description = "用户ID2")
    @NotNull(message = "ID必须填写")
    private Long id;
    @Schema(description = "名称")
    @NotBlank(message = "名称必须填写")
    @Size(min = 2, max = 10, message = "名称长度2-10")
    private String name;
    @Schema(description = "年龄")
    @Min(value = 0, message = "年龄需要大于0")
    @Max(value = 150, message = "年龄需要小于150")
    private Integer age;
    @Schema(description = "日期")
    private Date date;
}
