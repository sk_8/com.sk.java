package com.sk.web.support;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sk.util.SecurityDigest;
import com.sk.web.jackson.AlgorithmDecoding;
import com.sk.web.jackson.Base64;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;

/**
 * @author smy
 * {@code @date} 2023/1/29
 */
@Data
@Schema(description = "登录参数")
public class LoginParam {
    @Schema(example = "admin")
    private String username;
    @ToString.Exclude
    @JsonDeserialize(using = PWDecoding.class)
    @Schema(example = "abc")
    private String password1;
    @ToString.Exclude
    @Base64
    @Schema(example = "Y29tLnNrLmphdmE=")
    private String password2;

    @ToString.Exclude
    @AlgorithmDecoding(algorithm = "RSA/ECB/PKCS1Padding", key = "login.key1", digest = SecurityDigest.none)
    @Schema(example = "AJcFTS222G6ESi1Gt0FAJ0v8DJog6gMk2WAV6sV33bMKwKREusf9o2DiZxnHtAGvxmwtjLwQ1NPNPnRyIkv/piZNYGcKg1nbI+x1fvApGpXvE7mgfZFf0w89Jiq+b4c0nwNqYccq/r2KlT+kY5iUmBPURynwW1wtdHMHPvhS4DI=")
    private String password3;

}
