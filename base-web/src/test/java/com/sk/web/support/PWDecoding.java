package com.sk.web.support;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;

/**
 * @author smy
 * {@code @date} 2023/1/31
 */
@Slf4j
public class PWDecoding extends JsonDeserializer<String> {
    @Override
    public String deserialize(JsonParser parser, DeserializationContext context) throws IOException, JacksonException {
        String value = parser.getText();
        return value + "-abc";
    }
}
