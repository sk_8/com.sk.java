package com.sk.web.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.util.Base64Utils;

import java.io.IOException;

/**
 * @author smy
 */
public class Base64EncodeSerializer extends JsonSerializer<String> {

    @Override
    public void serialize(String value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeString(Base64Utils.encodeToString(value.getBytes()));
    }
}
