package com.sk.web.jackson;

import com.sk.util.TimeUtil;
import lombok.NonNull;
import org.springframework.core.convert.converter.Converter;
import org.springframework.util.StringUtils;

import java.util.Date;

/**
 * @author smy
 * {@code @date} 2022/12/18
 */
public class DateConverter implements Converter<String, Date> {
    @Override
    public Date convert(@NonNull String source) {
        if (!StringUtils.hasText(source)) {
            return null;
        }
        if (source.matches("^[0-9]+$")) {
            return new Date(Long.parseLong(source));
        } else {
            return TimeUtil.parse(source);
        }

    }
}
