package com.sk.web.jackson;

import com.fasterxml.jackson.annotation.JacksonAnnotationsInside;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.sk.util.SecurityDigest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author smy
 * {@code @date} 2023/3/13
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@JacksonAnnotationsInside
@JsonDeserialize(using = AlgorithmDeserializer.class)
public @interface AlgorithmDecoding {
    //解密算法:AES/ECB/PKCS5Padding|RSA/ECB/PKCS1Padding
    String algorithm();

    //密钥地址
    String key();

    //摘要算法:MD5|SHA-256
    SecurityDigest digest() default SecurityDigest.md5;


}
