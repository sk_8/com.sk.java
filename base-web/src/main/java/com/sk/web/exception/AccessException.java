package com.sk.web.exception;

import lombok.Getter;

/**
 * 权限控制类异常，HttpStatus=406
 * <p/>需要前端处理。
 *
 * @author smy
 */
@Getter
public class AccessException extends RuntimeException {
    /**
     * 错误编号。
     */
    private final int code;

    public AccessException(int code, String message) {
        super(message);
        this.code = code;
    }
}
