package com.sk.web.aop;

import lombok.extern.slf4j.Slf4j;

/**
 * 打印访问日志信息
 *
 * @author smy
 * {@code @date} 2022/11/18
 */
@Slf4j
public class AccessLogHandler {

    public void out(Object event) {
        log.info(event.toString());
    }

}
