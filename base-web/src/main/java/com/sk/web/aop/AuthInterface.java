package com.sk.web.aop;

/**
 * 权限验证接口
 *
 * @author smy
 * {@code @date} 2023/1/16
 */
public interface AuthInterface<T> {

    /**
     * 获取用户信息
     *
     * @return 用户基本信息
     */
    T getAccessInfo();

    /**
     * 校验用户与权限
     *
     * @param authCode 权限编号
     * @return 用户当前访问是否合法
     */
    default boolean check(String authCode) {
        return getAccessInfo() == null;
    }


}
