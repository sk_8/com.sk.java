package com.sk.web.aop;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author smy
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AccessPoint {

    Class<? extends AuthInterface>[] value() default AuthPublic.class;

    String authCode() default "";

    boolean args() default true;

    boolean result() default false;

}
