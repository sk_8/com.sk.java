package com.sk.web.aop;

import com.sk.util.ObjectUtil;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

/**
 * @author smy
 */
@Getter
@Setter
public class AccessLog {
    private String uri;
    private String method;
    private String args;
    private Object result;
    private Object access;
    private String errorType;
    private String errorMsg;

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("AccessLog{");
        appendStr(builder, "uri");
        appendStr(builder, "method");
        appendStr(builder, "args");
        appendStr(builder, "result");
        appendStr(builder, "access");
        appendStr(builder, "errorType");
        appendStr(builder, "errorMsg");
        return builder.append("}").toString();
    }

    private void appendStr(StringBuilder builder, String fieldName) {
        Object fieldValue = ObjectUtil.getValue(this, fieldName);
        if (Objects.nonNull(fieldValue)) {
            builder.append(fieldName).append("=").append(fieldValue).append(", ");
        }
    }
}
