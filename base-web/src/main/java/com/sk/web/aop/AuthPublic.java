package com.sk.web.aop;

import org.springframework.stereotype.Component;

/**
 * @author smy
 * {@code @date} 2023/1/16
 */
@Component
public class AuthPublic implements AuthInterface<Object> {

    @Override
    public Object getAccessInfo() {
        return null;
    }

    @Override
    public boolean check(String authCode) {
        return true;
    }


}
