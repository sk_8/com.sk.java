package com.sk.web;

import com.sk.util.SpringUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;

import java.net.InetAddress;

/**
 * @author smy
 * {@code @date} 2023/2/15
 */
@Slf4j
public class WebConfigUtil {
    /**
     * 用于自定义WebMvcConfigurer时
     *
     * @param prefix      url前缀
     * @param packageName 包名
     * @param configurer  spring路由配置
     */
    public static void addPathPrefix(String prefix, String packageName, PathMatchConfigurer configurer) {
        configurer.addPathPrefix(prefix, s -> s.isAnnotationPresent(RequestMapping.class) && s.getName().startsWith(packageName));
    }

    @SneakyThrows
    public static void outWebDevInfo() {
        String ip = InetAddress.getLocalHost().getHostAddress();
        String application = SpringUtil.getApplication();
        String port = SpringUtil.getProperty("server.port");
        String path = SpringUtil.getProperty("server.servlet.context-path", "");
        log.info("\n----------------------------------------------------------\n\t" +
                "Application " + application + " is running! Access URLs:\n\t" +
                "Local: \t\thttp://localhost:" + port + path + "/\n\t" +
                "External: \thttp://" + ip + ":" + port + path + "/\n\t" +
                "Swagger: \thttp://" + ip + ":" + port + path + "/swagger-ui/index.html\n" +
                "----------------------------------------------------------");
    }
}
