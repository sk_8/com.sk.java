# com.sk.java

本框架适用于基于java开发业务管理系统，核心架构java17+springboot3+jpa，扩展常用工具包和orm层的通用功能。

1. 属于企业级基础开发框架，包含orm/redis/web/log等定义，但不包含数据库任何实例、权限的定义
2. 使用jpa底层hibernate，没有Mybatis.xml文件，用简单的代码实现ORM层的关联查询、分组统计、复杂查询条件处理。
3. 该项目建议发布到内部maven仓库，然后在项目中配置引用，不建议与业务代码混一个项目。

### 演进历程

    2024年--v0.6
    重构orm层，优化PO与BO映射关系生成方法，完全匹配jsql查询全部能力
    项目开源，重新梳理各功能模块，逐步发布

    2021年--v0.4
    重够orm层，使用jsql方案，增强程序的灵活性，优化JOIN查询的能力
    支持springcloud
    扩展微信、支付宝、对象存储等云服务对接
    扩展文件素材处理，如二维码生成、图片、视频、音频、pdf、word等文件的常用工具

    2018年--v0.2
    升级java8，迎来首次项目实战，移除完全不适用的用户机构权限与spring-thymeleaf

    2017年--v0.1
    基于java7+springboot2.0搭建了0.1版，基于jpa开发orm层
    扩展了用户机构权限+spring-thymeleaf，缺代码生成器

### 目录介绍

[base-util](base-util) 常用工具包

[base-orm](base-orm) ORM工具，核心功能包，个人评价为最优秀的java-orm框架

[base-redis](base-redis) redis集成-仅一点,仅使用StringRedisTemplate，避免java序列化对平台整体造成不良影响

[base-web](base-web) web服务集成

[sk-starter](sk-starter) sk项目启动的快速集成包

### 备注

1. 项目重构后缺少完整测试,有问题欢迎沟通。联系开发者1024558667@qq.com,标题com.sk.java-XXX
2. 教程文档缺少,源码已尽量可读，参考单元测试应该能跑通所有流程。
3. 项目合适个人学习参考或者企业搭建自己的后端开发框架，大部分应该懂用户权限模块用别人的痛。
4. 个人推荐开发工具:IDEA+Alibaba Cloud Toolkit(IDEA插件)+PDManer(数据库设计+代码生成)，可以直接跑通数据设计、代码生成、开发、部署全流程。