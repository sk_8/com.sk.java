package com.sk.util;

import com.sk.util.model.ImgData;
import lombok.SneakyThrows;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Objects;

/**
 * 图片工具。
 *
 * @author smy
 * {@code @date} 2022/11/16
 */
public class ImgUtil {
    @SneakyThrows
    public static void image2File(BufferedImage image, File file) {
        FileUtil.mkdirs(file.getParentFile());
        ImageIO.write(image, FileUtil.getNameExtension(file.getName()), file);
    }

    @SneakyThrows
    public static byte[] image2Bytes(BufferedImage image, String ext) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ImageIO.write(image, ext, out);
        return out.toByteArray();
    }

    @SneakyThrows
    public static BufferedImage file2image(File file) {
        return ImageIO.read(file);
    }

    @SneakyThrows
    public static BufferedImage input2image(InputStream is) {
        return ImageIO.read(is);
    }

    @SneakyThrows
    public static BufferedImage url2image(URL url) {
        return ImageIO.read(url);
    }

    public static ImgData parse(BufferedImage bi) {
        ImgData imgData = new ImgData();
        imgData.setWidth(bi.getWidth());
        imgData.setHeight(bi.getHeight());
        imgData.setColorDepth(bi.getColorModel().getPixelSize());
        Object dpiW = bi.getProperty("javax_imageio_1.0.horizontal_dpi");
        if (dpiW != null && !Objects.equals(dpiW.getClass(), Object.class)) {
            imgData.setDpiW((Integer) dpiW);
        }
        Object dpiH = bi.getProperty("javax_imageio_1.0.vertical_dpi");
        if (dpiH != null && !Objects.equals(dpiH.getClass(), Object.class)) {
            imgData.setDpiH((Integer) dpiH);
        }
        return imgData;
    }

    public static ImgData parse(InputStream is) {
        return parse(input2image(is));
    }

    public static ImgData parse(File file) {
        return parse(file2image(file));
    }

    private static final SecureRandom RANDOM = new SecureRandom();
    private static final Integer LINE_NUM = 20;

    /**
     * 生成验证码
     *
     * @param w    图片宽
     * @param h    图片高
     * @param code 验证码
     * @return 验证码图片
     */
    public static BufferedImage gemCaptcha(int w, int h, String code) {
        int verifySize = code.length();
        BufferedImage image = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = image.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Color[] colors = new Color[5];
        Color[] colorSpaces = new Color[]{Color.WHITE, Color.CYAN,
                Color.GRAY, Color.LIGHT_GRAY, Color.MAGENTA, Color.ORANGE,
                Color.PINK, Color.YELLOW};
        float[] fractions = new float[colors.length];
        for (int i = 0; i < colors.length; i++) {
            colors[i] = colorSpaces[RANDOM.nextInt(colorSpaces.length)];
            fractions[i] = RANDOM.nextFloat();
        }
        Arrays.sort(fractions);
        // 设置边框色
        g2.setColor(Color.GRAY);
        g2.fillRect(0, 0, w, h);
        Color c = getRandColor(200, 250);
        // 设置背景色
        g2.setColor(c);
        g2.fillRect(0, 2, w, h - 4);
        //绘制干扰线
        // 设置线条的颜色
        g2.setColor(getRandColor(160, 200));
        for (int i = 0; i < LINE_NUM; i++) {
            int x = RANDOM.nextInt(w - 1);
            int y = RANDOM.nextInt(h - 1);
            int xl = RANDOM.nextInt(6) + 1;
            int yl = RANDOM.nextInt(12) + 1;
            g2.drawLine(x, y, x + xl + 40, y + yl + 20);
        }
        // 添加噪点
        // 噪声率
        float yawpRate = 0.05f;
        int area = (int) (yawpRate * w * h);
        for (int i = 0; i < area; i++) {
            int x = RANDOM.nextInt(w);
            int y = RANDOM.nextInt(h);
            int rgb = getRandomIntColor();
            image.setRGB(x, y, rgb);
        }
        // 使图片扭曲
        shear(g2, w, h, c);
        g2.setColor(getRandColor(100, 160));
        int fontSize = h - 4;
        Font font = new Font("Profont", Font.BOLD, fontSize);
        g2.setFont(font);
        char[] chars = code.toCharArray();
        for (int i = 0; i < verifySize; i++) {
            AffineTransform affine = new AffineTransform();
            affine.setToRotation(Math.PI / 4 * RANDOM.nextDouble() * (RANDOM.nextBoolean() ? 1 : -1), (w / verifySize) *
                    i + fontSize / 2, h / 2);
            g2.setTransform(affine);
            g2.drawChars(chars, i, 1, ((w - 10) / verifySize) * i + 5, h / 2 + fontSize / 2 - 10);
        }
        g2.dispose();
        return image;
    }

    private static Color getRandColor(int fc, int bc) {
        int r = fc + RANDOM.nextInt(bc - fc);
        int g = fc + RANDOM.nextInt(bc - fc);
        int b = fc + RANDOM.nextInt(bc - fc);
        return new Color(r, g, b);
    }

    private static int getRandomIntColor() {
        int[] rgb = getRandomRgb();
        int color = 0;
        for (int c : rgb) {
            color = color << 8;
            color = color | c;
        }
        return color;
    }

    private static int[] getRandomRgb() {
        int[] rgb = new int[3];
        for (int i = 0; i < rgb.length; i++) {
            rgb[i] = RANDOM.nextInt(255);
        }
        return rgb;
    }

    private static void shear(Graphics g, int w1, int h1, Color color) {
        shearX(g, w1, h1, color);
        shearY(g, w1, h1, color);
    }

    private static void shearX(Graphics g, int w1, int h1, Color color) {
        int period = RANDOM.nextInt(2);
        boolean borderGap = true;
        int frames = 1;
        int phase = RANDOM.nextInt(2);
        for (int i = 0; i < h1; i++) {
            double d = (double) (period >> 1)
                    * Math.sin((double) i / (double) period
                    + (6.2831853071795862D * (double) phase)
                    / (double) frames);
            g.copyArea(0, i, w1, 1, (int) d, 0);
            if (borderGap) {
                g.setColor(color);
                g.drawLine((int) d, i, 0, i);
                g.drawLine((int) d + w1, i, w1, i);
            }
        }

    }

    private static void shearY(Graphics g, int w1, int h1, Color color) {
        // 50
        int period = RANDOM.nextInt(40) + 10;
        boolean borderGap = true;
        int frames = 20;
        int phase = 7;
        for (int i = 0; i < w1; i++) {
            double d = (double) (period >> 1)
                    * Math.sin((double) i / (double) period
                    + (6.2831853071795862D * (double) phase)
                    / (double) frames);
            g.copyArea(i, 0, 1, h1, 0, (int) d);
            if (borderGap) {
                g.setColor(color);
                g.drawLine(i, (int) d, i, 0);
                g.drawLine(i, (int) d + h1, i, h1);
            }
        }
    }
}
