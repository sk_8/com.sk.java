package com.sk.util.convert;

import java.lang.annotation.*;

/**
 * 场景标签，根据不同场景可以公用一个对象。
 *
 * @author smy
 * {@code @date} 2023/12/15
 */
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ScenePoint.ScenePoints.class)
public @interface ScenePoint {


    String value() default "";

    Class<? extends DataConverter> converter() default DataDefaultConverter.class;

    String[] name() default "";

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.FIELD)
    @interface ScenePoints {
        ScenePoint[] value();
    }


}
