package com.sk.util.convert;

import org.jetbrains.annotations.NotNull;

/**
 * @author smy
 * {@code @date} 2023/12/15
 */
public class DataDefaultConverter implements DataConverter<Object, Object> {
    @Override
    public Object convert(@NotNull Object o) {
        return o;
    }
}
