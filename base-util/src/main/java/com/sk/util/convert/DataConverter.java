package com.sk.util.convert;

import org.jetbrains.annotations.NotNull;
import org.springframework.core.convert.converter.Converter;

import java.util.function.Function;

/**
 * 数据转换器接口
 * 1、同时继承Spring与java的标准转换接口
 * 2、拆分null与非null的处理逻辑。
 *
 * @author smy
 * {@code @date} 2023/12/15
 */
public interface DataConverter<IN, OUT> extends Converter<IN, OUT>, Function<IN, OUT> {
    @Override
    OUT convert(@NotNull IN in);

    default OUT resultForNull() {
        return null;
    }

    default OUT getResult(IN in) {
        return in == null ? resultForNull() : convert(in);
    }

    @Override
    default OUT apply(IN in) {
        return getResult(in);
    }
}
