package com.sk.util.convert;

import org.jetbrains.annotations.NotNull;

/**
 * @author smy
 * {@code @date} 2024/2/6
 */
public class IntConverter implements DataConverter<Object, Integer> {
    @Override
    public Integer convert(@NotNull Object o) {
        if (o instanceof Integer) {
            return (Integer) o;
        }
        if (o instanceof Number) {
            return ((Number) o).intValue();
        }
        if (o instanceof String) {
            return Integer.valueOf((String) o);
        }
        return null;
    }
}
