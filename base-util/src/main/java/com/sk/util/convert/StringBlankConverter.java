package com.sk.util.convert;

import com.sk.util.StringUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.util.StringUtils;

/**
 * @author wyh
 * {@code @date} 2023/5/11
 */
public class StringBlankConverter implements DataConverter<String, String> {

    @Override
    public String convert(@NotNull String source) {
        return StringUtil.removeWhitespace(source);
    }
}
