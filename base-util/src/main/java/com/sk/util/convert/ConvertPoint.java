package com.sk.util.convert;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 转换器标签
 *
 * @author smy
 * {@code @date} 2024/5/7
 */
@Target({ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface ConvertPoint {
    Class<? extends DataConverter> value() default DataDefaultConverter.class;
}
