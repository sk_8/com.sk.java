package com.sk.util.convert;

import org.jetbrains.annotations.NotNull;

/**
 * @author smy
 * {@code @date} 2023/12/19
 */
public class LongConverter implements DataConverter<Object, Long> {
    @Override
    public Long convert(@NotNull Object o) {
        if (o instanceof Long) {
            return (Long) o;
        }
        if (o instanceof Number) {
            return ((Number) o).longValue();
        }
        if (o instanceof String) {
            return Long.valueOf((String) o);
        }
        return null;
    }
}
