package com.sk.util;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Date;

/**
 * @author smy
 * {@code @date} 2023/2/1
 */
public class TimeUtil {
    public static final long MILLIS_PER_SECOND = 1000L;
    public static final long MILLIS_PER_MINUTE = 60000L;
    public static final long MILLIS_PER_HOUR = 3600000L;
    public static final long MILLIS_PER_DAY = 86400000L;

    private static final DateTimeFormatter formatterForParse = new DateTimeFormatterBuilder()
            .appendPattern("yyyy-MM-dd[' 'HH:mm:ss]")
            .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
            .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
            .parseDefaulting(ChronoField.SECOND_OF_MINUTE, 0)
            .toFormatter();


    public static Date parse(String dateStr) {
        LocalDateTime localDateTime = LocalDateTime.parse(dateStr, formatterForParse);
        ZoneId defaultZoneId = ZoneId.systemDefault();
        ZonedDateTime zdt = localDateTime.atZone(defaultZoneId);
        return Date.from(zdt.toInstant());
    }
}
