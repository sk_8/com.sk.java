package com.sk.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author smy
 * {@code @date} 2023/12/23
 */
public class FileMediaType {

    public static String toMediaType(String fileExt) {
        return map.getOrDefault(fileExt, "application/octet-stream");
    }

    public static String toMediaTypeByName(String filePath) {
        return toMediaType(FileUtil.getNameExtension(filePath));
    }


    private static Map<String, String> init() {
        Map<String, String> map = new ConcurrentHashMap<>();
        String[] lines = media_type_list.split(";");
        for (String line : lines) {
            if (line.isBlank()) {
                continue;
            }
            line = line.strip();
            String[] values = line.split("\\s+");
            for (int i = 1; i < values.length; i++) {
                map.put(values[i], values[0]);
            }
        }
        return map;
    }

    private static final Map<String, String> map = init();

    //直接解析nginx mime.types
    private static final String media_type_list = "\n" +
            "\n" +
            "    text/html                                        html htm shtml;\n" +
            "    text/css                                         css;\n" +
            "    text/xml                                         xml;\n" +
            "    image/gif                                        gif;\n" +
            "    image/jpeg                                       jpeg jpg;\n" +
            "    application/javascript                           js;\n" +
            "    application/atom+xml                             atom;\n" +
            "    application/rss+xml                              rss;\n" +
            "\n" +
            "    text/mathml                                      mml;\n" +
            "    text/plain                                       txt;\n" +
            "    text/vnd.sun.j2me.app-descriptor                 jad;\n" +
            "    text/vnd.wap.wml                                 wml;\n" +
            "    text/x-component                                 htc;\n" +
            "\n" +
            "    image/avif                                       avif;\n" +
            "    image/png                                        png;\n" +
            "    image/svg+xml                                    svg svgz;\n" +
            "    image/tiff                                       tif tiff;\n" +
            "    image/vnd.wap.wbmp                               wbmp;\n" +
            "    image/webp                                       webp;\n" +
            "    image/x-icon                                     ico;\n" +
            "    image/x-jng                                      jng;\n" +
            "    image/x-ms-bmp                                   bmp;\n" +
            "\n" +
            "    font/woff                                        woff;\n" +
            "    font/woff2                                       woff2;\n" +
            "\n" +
            "    application/java-archive                         jar war ear;\n" +
            "    application/json                                 json;\n" +
            "    application/mac-binhex40                         hqx;\n" +
            "    application/msword                               doc;\n" +
            "    application/pdf                                  pdf;\n" +
            "    application/postscript                           ps eps ai;\n" +
            "    application/rtf                                  rtf;\n" +
            "    application/vnd.apple.mpegurl                    m3u8;\n" +
            "    application/vnd.google-earth.kml+xml             kml;\n" +
            "    application/vnd.google-earth.kmz                 kmz;\n" +
            "    application/vnd.ms-excel                         xls;\n" +
            "    application/vnd.ms-fontobject                    eot;\n" +
            "    application/vnd.ms-powerpoint                    ppt;\n" +
            "    application/vnd.oasis.opendocument.graphics      odg;\n" +
            "    application/vnd.oasis.opendocument.presentation  odp;\n" +
            "    application/vnd.oasis.opendocument.spreadsheet   ods;\n" +
            "    application/vnd.oasis.opendocument.text          odt;\n" +
            "    application/vnd.openxmlformats-officedocument.presentationml.presentation\n" +
            "                                                     pptx;\n" +
            "    application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\n" +
            "                                                     xlsx;\n" +
            "    application/vnd.openxmlformats-officedocument.wordprocessingml.document\n" +
            "                                                     docx;\n" +
            "    application/vnd.wap.wmlc                         wmlc;\n" +
            "    application/wasm                                 wasm;\n" +
            "    application/x-7z-compressed                      7z;\n" +
            "    application/x-cocoa                              cco;\n" +
            "    application/x-java-archive-diff                  jardiff;\n" +
            "    application/x-java-jnlp-file                     jnlp;\n" +
            "    application/x-makeself                           run;\n" +
            "    application/x-perl                               pl pm;\n" +
            "    application/x-pilot                              prc pdb;\n" +
            "    application/x-rar-compressed                     rar;\n" +
            "    application/x-redhat-package-manager             rpm;\n" +
            "    application/x-sea                                sea;\n" +
            "    application/x-shockwave-flash                    swf;\n" +
            "    application/x-stuffit                            sit;\n" +
            "    application/x-tcl                                tcl tk;\n" +
            "    application/x-x509-ca-cert                       der pem crt;\n" +
            "    application/x-xpinstall                          xpi;\n" +
            "    application/xhtml+xml                            xhtml;\n" +
            "    application/xspf+xml                             xspf;\n" +
            "    application/zip                                  zip;\n" +
            "\n" +
            "    application/octet-stream                         bin exe dll;\n" +
            "    application/octet-stream                         deb;\n" +
            "    application/octet-stream                         dmg;\n" +
            "    application/octet-stream                         iso img;\n" +
            "    application/octet-stream                         msi msp msm;\n" +
            "\n" +
            "    audio/midi                                       mid midi kar;\n" +
            "    audio/mpeg                                       mp3;\n" +
            "    audio/ogg                                        ogg;\n" +
            "    audio/x-m4a                                      m4a;\n" +
            "    audio/x-realaudio                                ra;\n" +
            "\n" +
            "    video/3gpp                                       3gpp 3gp;\n" +
            "    video/mp2t                                       ts;\n" +
            "    video/mp4                                        mp4;\n" +
            "    video/mpeg                                       mpeg mpg;\n" +
            "    video/quicktime                                  mov;\n" +
            "    video/webm                                       webm;\n" +
            "    video/x-flv                                      flv;\n" +
            "    video/x-m4v                                      m4v;\n" +
            "    video/x-mng                                      mng;\n" +
            "    video/x-ms-asf                                   asx asf;\n" +
            "    video/x-ms-wmv                                   wmv;\n" +
            "    video/x-msvideo                                  avi;\n" +
            "\n";
}
