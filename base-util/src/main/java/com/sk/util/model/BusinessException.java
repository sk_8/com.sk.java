package com.sk.util.model;

import lombok.Getter;

/**
 * 业务异常，HttpStatus=200。
 * <p/>指定业务模块，用户操作导致问题，比如验证码错误、账号密码错误
 *
 * @author smy
 */
@Getter
public class BusinessException extends RuntimeException {
    /**
     * 错误编号。
     */
    private final int code;


    private Object data;

    public BusinessException(int code, String message) {
        super(message);
        this.code = code;

    }

    public BusinessException(int code, String message,Object data) {
        super(message);
        this.code = code;
        this.data = data;
    }


}
