package com.sk.util.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author smy
 * {@code @date} 2023/12/27
 */
@Data
@Schema(description = "文件数据模型")
public class FileData {
    @Schema(description = "文件名")
    private String name;
    @Schema(description = "扩展名")
    private String ext;
    @Schema(description = "文件大小")
    private Long size;
    @Schema(description = "文件md5")
    private String md5;
}
