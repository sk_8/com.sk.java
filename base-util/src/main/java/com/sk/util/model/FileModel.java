package com.sk.util.model;

import com.sk.util.FileUtil;
import lombok.Data;
import lombok.SneakyThrows;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author smy
 * {@code @date} 2023/12/27
 */
@Data
public class FileModel extends FileData {
    private File file;


    protected FileModel(File file) {
        this.file = file;
        setName(file.getName());
        setExt(FileUtil.getNameExtension(getName()));
    }

    public static FileModel gen(File file) {
        return new FileModel(file);
    }

    public static FileModel gen(String dir, String path) {
        return new FileModel(new File(dir, path));
    }

    public InputStream getInputStream() {
        return FileUtil.inputStream(file);
    }

    public OutputStream getOutputStream() {
        return FileUtil.outputStream(file);
    }

    public FileModel newSub(String ext) {
        if (!ext.contains(".")) {
            ext = "." + ext;
        }
        return new FileModel(new File(file.getPath() + ext));
    }


}
