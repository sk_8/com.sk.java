package com.sk.util.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author smy
 * {@code @date} 2023/12/22
 */
@Schema(description = "图片信息")
@Data
public class ImgData {
    @Schema(description = "图片宽度")
    private Integer width;
    @Schema(description = "图片高度")
    private Integer height;
    @Schema(description = "色深,8/24/32")
    private Integer colorDepth;
    @Schema(description = "水平dpi")
    private Integer dpiW = 72;
    @Schema(description = "垂直dpi")
    private Integer dpiH = 72;


    public Integer sizeUntrue() {
        return width * height * colorDepth / 8 / dpiW / dpiH;
    }
}
