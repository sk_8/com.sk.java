package com.sk.util.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 远程服务调用异常。
 *
 * @author smy
 * {@code @date} 2023/2/10
 */
@AllArgsConstructor
@Getter
public class BusinessRemoteException extends RuntimeException {
    private Result<?> result;

    public BusinessRemoteException(int code, String message, String application) {
        result = new Result<>(code, message, application, null);
    }

    public static void check(Result<?> result) {
        if (result.getCode() != 0) {
            throw new BusinessRemoteException(result);
        }
    }
}
