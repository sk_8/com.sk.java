package com.sk.util.model;

/**
 * @author smy
 * {@code @date} 2022/11/28
 */
public interface EConsumer<T> {
    void accept(T t) throws Exception;
}
