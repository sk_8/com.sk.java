package com.sk.util.model;

import java.util.List;

/**
 * @author smy
 * {@code @date} 2023/1/18
 */
public interface ITree<T extends ITree> {

    Object getId();

    Object getParentId();

    List<T> getChildren();
}
