package com.sk.util;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.security.MessageDigest;

/**
 * 安全摘要方案:MD5,SHA-256
 *
 * @author smy
 * {@code @date} 2023/3/14
 */
@AllArgsConstructor
public enum SecurityDigest {
    md5("MD5"),
    sha256("SHA-256"),
    md2("MD2"),
    sha1("SHA-1"),
    sha224("SHA-224"),
    sha384("SHA-384"),
    sha512("SHA-512"),
    sha512_224("SHA-512/224"),
    sha512_256("SHA-512/256"),
    none(null) {
        @Override
        public String encode(byte[] bytes) {
            return new String(bytes);
        }

        @Override
        public String encode(String value) {
            return value;
        }
    };
    private final String algorithm;

    @SneakyThrows
    public String encode(byte[] bytes) {
        MessageDigest digest = MessageDigest.getInstance(algorithm);
        digest.update(bytes);
        return SecurityUtil.toHex(digest.digest());
    }

    public String encode(String value) {
        return encode(value.getBytes());
    }


}
