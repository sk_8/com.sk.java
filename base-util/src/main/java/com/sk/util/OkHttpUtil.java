package com.sk.util;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import okhttp3.*;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Map;

/**
 * OKHttp工具
 *
 * @author smy
 * {@code @date} 2023/1/8
 */
@Slf4j
public class OkHttpUtil {


    public static Response get(OkHttpClient client, String url) throws IOException {
        return get(client, url, Collections.emptyMap());
    }

    public static Response get(OkHttpClient client, String url, Map<String, String> headers) throws IOException {
        Request.Builder builder = new Request.Builder().url(url);
        headers.forEach(builder::header);
        Request request = builder.build();
        return client.newCall(request).execute();
    }

    public static final MediaType MediaTypeJson = MediaType.parse("application/json; charset=utf-8");

    public static RequestBody json(Object data) {
        return RequestBody.create(JSONObject.toJSONString(data), MediaTypeJson);
    }

    public static Response post(OkHttpClient client, String url, RequestBody body) throws IOException {
        Request.Builder builder = new Request.Builder().url(url);
        Request request = builder.post(body).build();
        return client.newCall(request).execute();
    }

    public static Response post(OkHttpClient client, String url, Map<String, String> headers, RequestBody body) throws IOException {
        Request.Builder builder = new Request.Builder().url(url);
        headers.forEach(builder::header);
        Request request = builder.post(body).build();
        return client.newCall(request).execute();
    }

    public static OkHttpClient sslIgnoreClient() {
        X509TrustManager manager = getX509TrustManager();
        SSLSocketFactory factory = getSocketFactory(manager);
        return new OkHttpClient.Builder().sslSocketFactory(factory, manager).hostnameVerifier((hostname, session) -> true).build();
    }

    public static OkHttpClient client() {
        return new OkHttpClient.Builder().build();
    }

    public static OkHttpClient client(boolean sslIgnore) {
        return sslIgnore ? sslIgnoreClient() : client();
    }

    public static X509TrustManager getX509TrustManager() {
        return new X509TrustManager() {
            @Override
            public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

            }

            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        };
    }

    public static SSLSocketFactory getSocketFactory(TrustManager manager) {
        SSLSocketFactory socketFactory = null;
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, new TrustManager[]{manager}, new SecureRandom());
            socketFactory = sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }
        return socketFactory;
    }
}
