package com.sk.util;

import com.sk.util.model.User;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author smy
 * {@code @date} 2024/4/17
 */
class ObjectUtilTest {
    @Test
    public void field() throws NoSuchFieldException, IllegalAccessException {
        User user = new User();
        Field id = User.class.getDeclaredField("id");
        id.setAccessible(true);
        id.set(user, 1);
        System.out.println(user);
    }

    @Test
    public void field2() {
        System.out.println(ObjectUtil.getFields(User.class));
    }

    @Test
    public void method() throws NoSuchMethodException {
        System.out.println(User.class.getMethod("getId"));
        System.out.println(User.class.getMethod("setId", Integer.class));
        System.out.println(ObjectUtil.getMethodByName(User.class, "setId"));
    }

    @Test
    void testArray() {
        System.out.println(ObjectUtil.arrayContains("a", new String[]{"a", "b", "c"}));
    }
}