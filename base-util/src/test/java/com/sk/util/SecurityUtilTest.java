package com.sk.util;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.util.Base64Utils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.io.File;
import java.security.*;

/**
 * @author smy
 * {@code @date} 2023/3/14
 */
@Slf4j
class SecurityUtilTest {
    public static void printBytes(byte[] bytes){
        System.out.println("=========utf8");
        System.out.println(new String(bytes));
        System.out.println("=========hex");
        System.out.println(SecurityUtil.toHex(bytes));
        System.out.println("=========base64");
        System.out.println(Base64Utils.encodeToString(bytes));
    }

    public static String generateRandomString(int length) {
        SecureRandom secureRandom = new SecureRandom();
        byte[] randomBytes = new byte[length / 2];
        secureRandom.nextBytes(randomBytes);
        return SecurityUtil.toHex(randomBytes);
    }

    public static byte[] generateRandomBytes(int length) {
        SecureRandom secureRandom = new SecureRandom();
        byte[] randomBytes = new byte[length];
        secureRandom.nextBytes(randomBytes);
        return randomBytes;
    }

    @Test
    void testSM4() throws Exception {
//        Security.addProvider(new BouncyCastleProvider());
        byte[] opts = generateRandomBytes(16);
        String opt = SecurityUtil.toHex(opts);
        System.out.println("==opt");
        System.out.println(opt);
        System.out.println(opt.length());
        byte[] ivs = generateRandomBytes(16);
        String algorithm = "SM4";
        String aat = "SM4/GCM/NoPadding";
        Key secretKeySpec = SecurityUtil.loadSecretKey(algorithm, opts);
        Cipher cipher = Cipher.getInstance(aat, "BC");
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, new IvParameterSpec(ivs));
        String value = "测试";
        byte[] ciphertextBytes = cipher.doFinal(value.getBytes());
        String r = SecurityUtil.toHex(ciphertextBytes);
        System.out.println(r);
    }


    @Test
    void testAEC() {
        String algorithm = "AES";
        String aat = "AES/ECB/PKCS5Padding";
        SecretKey secretKey = SecurityUtil.genSecretKey(algorithm, 128);
        String key0 = SecurityUtil.key2base64(secretKey);
        System.out.println(key0);
        String value0 = "测试";
        Key key = SecurityUtil.loadSecretKey(algorithm, key0);
        byte[] bytes = SecurityUtil.encodeByCipher(aat, value0.getBytes(), key);
        printBytes(bytes);
        String value1 = new String(SecurityUtil.decodeByCipher(aat, bytes, key));
        System.out.println(value1);
    }

    @Test
    void testRAS() {
        String algorithm = "RSA";
        String aat = "RSA/ECB/PKCS1Padding";
        KeyPair keyPair = SecurityUtil.getKeyPair(algorithm, 1024);
        String key1 = SecurityUtil.key2base64(keyPair.getPublic());
        String key2 = SecurityUtil.key2base64(keyPair.getPrivate());
        log.info("PublicKey={}", key1);
        log.info("PrivateKey={}", key2);
        PublicKey publicKey = SecurityUtil.loadPublicKey(algorithm, key1);
        PrivateKey privateKey = SecurityUtil.loadPrivateKey(algorithm, key2);
        String value0 = "11``qqq";
        String value1 = SecurityUtil.encodeByCipher(aat, value0, publicKey);
        log.info("value1={}", value1);
        String value2 = SecurityUtil.decodeByCipher(aat, value1, privateKey);
        log.info("value2={}", value2);
    }

    @Test
    void testRAS2() {
        //        String keyValue = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAJff4KJ5CoTdeccdN13kvcUuueZhu51nSGvPemcS3I1GSZ+ZHL71mfoI5JkyUJxt6+MsyMcBYUt9Taz9k6uwBBumFigxejR27t9crfJbayjCfiDlBJfqKKR5ax143aVuRP+NWyuqXzRqMbbmzBD/GFsYcEQCsJsls60ucvBhAoM/AgMBAAECgYB0x6/6HmqJOae/BGcjapTFPyWw0WurH9TfGV18TlOKygYok1o4N6/bQ+VLKxYzAo7EUxZbd2Mx4brNjvpypNiyliqCFlL2uqd1zosgBmSJeMkBqbRcpR1K2QxbLNdRRK7SP3tbe21qFw2qX1rWhfyyWUSN8AtnUnZE+/j+Xw6w6QJBAPm0c4rlcSEIE0DFkA9VTQkrvxmtNhw1Vj
        //        +xW2qvMQx3m0ABFgdSQYXbMj" +
        //                "/93pmE8s9c9W5QFPnk/DdQ7s3csJUCQQCbtAwYRdgHU79KvxCbZxrEFYPxqk6uQZRZW7G+Bo4gueUUlYgcwR2vIB5dQALqDDWxdhFLsI29uW3M7swbVsuDAkEAgWViToKxSgWuPG1kRp3UasExqTOaC6oUBLN6hOE+EJDWcadea/LTI3NAKer7dCBx1sT8Jer3w7tGe7/D0AfhtQJAbhmEq+1HcvbvP2WX0qu3q4wjPtHUzTemNQG3z81G1zFcIZATLXIJEXq+veWzqdh0Z0Y7uK/hqpTiZZU+Ut6vDQJAJvtDyRpPvETbWaAVCyxkY8xiTiVuow9MW5OFLFxIaTWb7vrQRT+GxB/xIsED+z9KyXFeVINuyRQXD29Hdw54Dg==";
        String keyValue = FileUtil.readString(new File("/data/tools/PrivateKey.txt"));
        String value1 = "Azcs6vrdX1s6v41+5sdVz9e9c5Ce5f1X0wngXRUCH8koREkZKIBHmV3v0OCH+0cswxAkabwEufnZVZ+4+kqyFD6G6cMPqYbKj5O+lbbHY2l218jFKJJgVTF4ruxihevl9NqwXi8jjVQSY85xOw4YdaRPgT26j9/ky+THFL4Ct7E=";
        String algorithm = "RSA/ECB/PKCS1Padding";
        Key key = SecurityUtil.loadDecodeKey(SecurityUtil.algorithm(algorithm), keyValue);
        String value = SecurityUtil.decodeByCipher(algorithm, value1, key);
        System.out.println(value);
    }

    @Test
    void testSignByRSA() {
        String algorithm = "RSA";
        String as = "MD5withRSA";
        KeyPair keyPair = SecurityUtil.getKeyPair(algorithm, 1024);
        String key1 = SecurityUtil.key2base64(keyPair.getPublic());
        String key2 = SecurityUtil.key2base64(keyPair.getPrivate());
        PublicKey publicKey = SecurityUtil.loadPublicKey(algorithm, key1);
        PrivateKey privateKey = SecurityUtil.loadPrivateKey(algorithm, key2);
        String value0 = "测试";
        byte[] sign = SecurityUtil.sign(as, value0.getBytes(), privateKey);
        boolean r = SecurityUtil.verify(as, value0.getBytes(), sign, publicKey);
        System.out.println(r);
    }

    @Test
    void testIV() {
        String algorithm = "AES";
        String aat = "AES/CBC/PKCS5Padding";
        SecretKey secretKey = SecurityUtil.genSecretKey(algorithm, 128);
        String key0 = SecurityUtil.key2base64(secretKey);
        System.out.println("=========key0");
        System.out.println(key0);
        String iv = SecurityUtil.genRandom2Base64(16);
        System.out.println("=========iv");
        System.out.println(iv);
        String value0 = "测试";
        Key key = SecurityUtil.loadSecretKey(algorithm, key0);
        byte[] bytes = SecurityUtil.encodeByCipher(aat, value0.getBytes(), Base64Utils.decodeFromString(iv), key);
        printBytes(bytes);
        String value1 = new String(SecurityUtil.decodeByCipher(aat, bytes, Base64Utils.decodeFromString(iv), key));
        System.out.println(value1);
    }
}
