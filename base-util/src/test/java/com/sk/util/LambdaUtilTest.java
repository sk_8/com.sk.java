package com.sk.util;

import com.alibaba.fastjson.JSON;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author smy
 * {@code @date} 2023/1/2
 */
public class LambdaUtilTest {

    @Test
    public void listGroup() {
        List<String> list = Arrays.asList("a", "a", "b", "c");
        Map<String, List<String>> map1 = LambdaUtil.listGroup(list, k -> k);
        System.out.println(JSON.toJSONString(map1));
        // {"a":["a","a"],"b":["b"],"c":["c"]}
        Map<String, AtomicInteger> map2 = LambdaUtil.listGroup(list, String::valueOf, o1 -> new AtomicInteger(), (o1, o2) -> o2.incrementAndGet());
        System.out.println(JSON.toJSONString(map2));
        //{"a":2,"b":1,"c":1}
    }


    @Test
    void compare() {
        List<String> list1 = Arrays.asList("a", "b", "c");
        List<String> list2 = Arrays.asList("b", "c", "d");
        LambdaUtil.compare(list1, list2, String::equals, o1 -> {
            System.out.println("o1=" + o1);
        }, o2 -> {
            System.out.println("o2=" + o2);
        }, (o1, o2) -> {
            System.out.println("eq=" + o1);
        });
        /*
          输出结果：
          o1=a
          eq=b
          eq=c
          o2=d
          o1可执行添加处理，o2执行移除处理，eq执行修改处理
         */
    }

    @Test
    void deep() {

    }
}
