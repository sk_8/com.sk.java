package com.sk.util;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author smy
 * {@code @date} 2024/4/22
 */
@Slf4j
class StringUtilTest {
    @Test
    void testRemoveWhitespace() {
        String text = "  9137\u200B0213MA7    FM WH  K26​555";
        System.out.println(StringUtil.removeWhitespace(text));
    }

    static Stream<Arguments> dataRuleCheck() {
        return Stream.of(
                Arguments.of("sys:user:add", "*", true),
                Arguments.of("sys:user:add", "sys:*", true),
                Arguments.of("sys:user:add", "data:*", false)
        );
    }

    @ParameterizedTest
    @MethodSource("dataRuleCheck")
    void testRuleCheck(String code, String rule, boolean bool) {
        boolean result = StringUtil.ruleCheck(code, rule);
        assertEquals(result, bool);
        log.info("RuleCheck({},{})={}", code, rule, bool);
    }

    @Test
    void pattern() {
        String template = "用户<${name}>的验证码=${code}";
        Map<String, Object> map = new HashMap<>();
        map.put("name", "张三");
        map.put("code", "1234");
        System.out.println(StringUtil.pattern(template,map));
    }
}
