package com.sk.util.model;

import com.sk.util.convert.ScenePoint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author smy
 * {@code @date} 2023/1/19
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @ScenePoint
    private Integer id;

    @ScenePoint(name = "nickName")
    @ScenePoint(name = "userName", value = "sys")
    private String name;

    private String password;
}
